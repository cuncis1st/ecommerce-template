package com.example.cuncis.ecommerce.fragment;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cuncis.ecommerce.MainActivity;
import com.example.cuncis.ecommerce.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvoiceFragment extends Fragment {

    TextView textHistori;
    LinearLayout linearHistori;
    ListView listHistori;
    SimpleAdapter simpleAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>(); //biar bisa mengisi data product

    public InvoiceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_invoice, container, false);
        textHistori = view.findViewById(R.id.text_histori);
        listHistori = view.findViewById(R.id.list_invoice);
        linearHistori = view.findViewById(R.id.linear_histori);

        textHistori.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);    //memberikan efek (garis bawah)
        textHistori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (linearHistori.getVisibility() == View.GONE) {
                    linearHistori.setVisibility(View.VISIBLE);
                    //mengganti (arrow_down) -> (arrow_up)
                    textHistori.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.arrow_up_float, 0);
                } else {
                    linearHistori.setVisibility(View.GONE);
                    textHistori.setCompoundDrawablesWithIntrinsicBounds(0, 0, android.R.drawable.arrow_down_float, 0);
                }
            }
        });

        setHistori();

        return view;
    }

    private void setHistori() {
        for (int i = 1; i <= 10; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("date", "August, " + String.valueOf(i) + " 2018");
            arrayList.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(getActivity(), arrayList, R.layout.row_histori,
                new String[]{"date"},
                new int[]{R.id.text_date});
        listHistori.setAdapter(simpleAdapter);
        listHistori.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String date = ((TextView) view.findViewById(R.id.text_date)).getText().toString();
                Toast.makeText(getActivity(), date, Toast.LENGTH_SHORT).show();

                MainActivity.tabLayout.setupWithViewPager(MainActivity.viewPager);
                MainActivity.viewPager.setCurrentItem(0);

            }
        });
    }

}
