package com.example.cuncis.ecommerce;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StoreActivity extends AppCompatActivity {

    GridView gridProduk;
    SimpleAdapter simpleAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gridProduk = findViewById(R.id.grid_product);
        setProduct();

    }

    private void setProduct() {
        for (int i = 1; i <= 10; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("price", "Rp. " + String.valueOf(i) + "0.000");
            hashMap.put("seller", "Lazday Indonesia");
            arrayList.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(this, arrayList, R.layout.row_product,
                new String[]{"price", "seller"},
                new int[]{R.id.text_price, R.id.text_seller});
        gridProduk.setAdapter(simpleAdapter);
        gridProduk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String harga = ((TextView)view.findViewById(R.id.text_price)).getText().toString();
                Toast.makeText(getApplicationContext(), harga, Toast.LENGTH_SHORT).show();

                //pindah activity
                startActivity(new Intent(StoreActivity.this, DetailActivity.class));
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();   //menutup activty
        return true;
    }

}
