package com.example.cuncis.ecommerce.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.cuncis.ecommerce.fragment.PurchaseFragment;
import com.example.cuncis.ecommerce.fragment.SalesFragment;

public class TransPagerAdapter extends FragmentPagerAdapter {


    private final String[] PAGE_TITLES = new String[]{
            "Pembelian", "Penjualan"
    };

    private final Fragment[] PAGES = new Fragment[]{
            new PurchaseFragment(), new SalesFragment()
    };

    public TransPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return PAGES[position];
    }

    @Override
    public int getCount() {
        return PAGES.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return PAGE_TITLES[position];
    }
}
