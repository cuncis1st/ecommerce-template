package com.example.cuncis.ecommerce.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.cuncis.ecommerce.fragment.AddressFragment;
import com.example.cuncis.ecommerce.fragment.CategoryFragment;
import com.example.cuncis.ecommerce.fragment.InvoiceFragment;
import com.example.cuncis.ecommerce.fragment.ProductFragment;

public class StatusPagerAdapter extends FragmentPagerAdapter {

    private final String[] PAGE_TITLES = new String[] {
            "STATUS", "DETAIL"
    };

    //page that want to display
    private final Fragment[] PAGES = new Fragment[]{
            new InvoiceFragment(), new AddressFragment()
    };

    public StatusPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return PAGES[position];

    }
    @Override
    public int getCount() {
        return PAGES.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return PAGE_TITLES[position];
    }
}
