package com.example.cuncis.ecommerce.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cuncis.ecommerce.MainActivity;
import com.example.cuncis.ecommerce.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {

    ListView listCategory;
    SimpleAdapter simpleAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>(); //biar bisa mengisi data product

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false);
        listCategory = view.findViewById(R.id.list_category);
        setCategory();

        return view;
}

    private void setCategory() {
        for (int i = 1; i <= 10; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("title", "CATEGORY - " + String.valueOf(i));
            arrayList.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(getActivity(), arrayList, R.layout.row_category,
                new String[]{"title"},
                new int[]{R.id.text_title});
        listCategory.setAdapter(simpleAdapter);
        listCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String kategori = ((TextView)view.findViewById(R.id.text_title)).getText().toString();
                Toast.makeText(getActivity(), kategori, Toast.LENGTH_SHORT).show();

                MainActivity.tabLayout.setupWithViewPager(MainActivity.viewPager);
                MainActivity.viewPager.setCurrentItem(0);

            }
        });
    }

}
