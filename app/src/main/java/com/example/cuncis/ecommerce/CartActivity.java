package com.example.cuncis.ecommerce;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.cuncis.ecommerce.adapter.CartAdapter;
import com.example.cuncis.ecommerce.model.Cart;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity {
    SwipeRefreshLayout swipe;

    RecyclerView recyclerView;
    ArrayList<Cart> cartList;
    CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipe = findViewById(R.id.swipe);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        cartList = new ArrayList<>();
        setCart();

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cartList.clear();
                recyclerView.setAdapter(null);
                setCart();
            }
        });

    }

    private void setCart() {
        for (int i = 0; i < 3; i++) {
            Cart cart = new Cart();
            cartList.add(cart);
        }

        cartAdapter = new CartAdapter(CartActivity.this, cartList);
        recyclerView.setAdapter(cartAdapter);

        swipe.setRefreshing(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();   //menutup activty
        return true;
    }
}
