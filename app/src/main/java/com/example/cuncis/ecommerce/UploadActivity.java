package com.example.cuncis.ecommerce;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class UploadActivity extends AppCompatActivity {

    Spinner spinKategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //array
        String[] contents = {"King", "Queen", "Jack"};
        spinKategori = findViewById(R.id.spin_category);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, contents);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinKategori.setAdapter(arrayAdapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();   //menutup activty
        return true;
    }

}
