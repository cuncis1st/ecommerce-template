package com.example.cuncis.ecommerce.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cuncis.ecommerce.DetailActivity;
import com.example.cuncis.ecommerce.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends Fragment {

    GridView gridProduct;
    SimpleAdapter simpleAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>(); //biar bisa mengisi data product

    public ProductFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_product, container, false);
        gridProduct = view.findViewById(R.id.grid_product);
        setProduct();

        return view;
    }

    //klo di activity pake getApplicationContext, kalo di fragment getActivity
    private void setProduct() {
        for (int i = 1; i <= 10; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("price", "Rp. " + String.valueOf(i) + "0.000");
            hashMap.put("seller", "Lazday Indonesia");
            arrayList.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(getActivity(), arrayList, R.layout.row_product,
                new String[]{"price", "seller"},
                new int[]{R.id.text_price, R.id.text_seller});
        gridProduct.setAdapter(simpleAdapter);
        gridProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String harga = ((TextView)view.findViewById(R.id.text_price)).getText().toString();
                Toast.makeText(getActivity(), harga, Toast.LENGTH_SHORT).show();

                //pindah activity
                startActivity(new Intent(getActivity(), DetailActivity.class));
            }
        });
    }

}



















