package com.example.cuncis.ecommerce.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cuncis.ecommerce.R;
import com.example.cuncis.ecommerce.StatusActivity;
import com.example.cuncis.ecommerce.adapter.SalesAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalesFragment extends Fragment {

    ListView listSales;
    SalesAdapter salesAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>(); //biar bisa mengisi data product

    public SalesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sales, container, false);
        listSales = view.findViewById(R.id.list_sales);
        setSales();

        return view;
    }

    private void setSales() {

        String[] product = {"product1", "product2"};
        String[] title = {"DVD Tutorial Android Pemula", "DVD Tutorial Android Menengah"};
        String[] date = {"Oct, 13 2018 - 10.30pm", "Oct, 14 2018 - 10.30pm"};

        //status
        String[] shipment = {"dikirim", "belum dikirim"};
        String[] drop = {"diterima", "ditolak"};
        String[] finish = {"selesai", "pengembalian"};

        for (int i = 0; i < title.length; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("product", product[i]);
            hashMap.put("title", title[i]);
            hashMap.put("date", date[i]);
            hashMap.put("shipment", shipment[i]);
            hashMap.put("drop", drop[i]);
            hashMap.put("finish", finish[i]);
            arrayList.add(hashMap);
        }

        salesAdapter = new SalesAdapter(getActivity(), arrayList, R.layout.row_purchase,
                new String[]{"product", "title", "date", "shipment", "drop", "finish"},
                new int[]{R.id.img_product, R.id.text_title, R.id.text_desc, R.id.img_car, R.id.img_drop, R.id.img_finish});
        listSales.setAdapter(salesAdapter);
        listSales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String harga = ((TextView)view.findViewById(R.id.text_title)).getText().toString();
                Toast.makeText(getActivity(), harga, Toast.LENGTH_SHORT).show();

                startActivity(new Intent(getActivity(), StatusActivity.class));

            }
        });
    }

}
