package com.example.cuncis.ecommerce;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NotifActivity extends AppCompatActivity {

    ListView listNotif;
    SimpleAdapter simpleAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>(); //biar bisa mengisi data productListView listNotif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listNotif = findViewById(R.id.list_notif);
        setNotif();

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void setNotif() {
        for (int i = 1; i <= 10; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("title", "Loremm " + String.valueOf(i) + "0.000");
            arrayList.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(NotifActivity.this, arrayList, R.layout.row_notif,
                new String[]{"title"},
                new int[]{R.id.text_title});
        listNotif.setAdapter(simpleAdapter);
        listNotif.setOnItemClickListener(   new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String title = ((TextView)view.findViewById(R.id.text_title)).getText().toString();
                Toast.makeText(NotifActivity.this, title, Toast.LENGTH_SHORT).show();

                TextView textDesc = view.findViewById(R.id.text_desc);
                if (textDesc.getVisibility() == View.GONE) {
                    textDesc.setVisibility(View.VISIBLE);
                } else {
                    textDesc.setVisibility(View.GONE);
                }
            }
        });
    }
}
