package com.example.cuncis.ecommerce.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cuncis.ecommerce.R;
import com.example.cuncis.ecommerce.StatusActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PurchaseFragment extends Fragment {

    ListView listPurchase;
    SimpleAdapter simpleAdapter;
    List<HashMap<String, String>> arrayList = new ArrayList<>(); //biar bisa mengisi data product

    public PurchaseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_purchase, container, false);
        listPurchase = view.findViewById(R.id.list_purchase);
        setPurchase();

        return view;
    }

    private void setPurchase() {
        for (int i = 1; i <= 10; i++) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("price", "Rp. " + String.valueOf(i) + "0.000");
            arrayList.add(hashMap);
        }

        simpleAdapter = new SimpleAdapter(getActivity(), arrayList, R.layout.row_purchase,
                new String[]{"price"},
                new int[]{R.id.text_desc});
        listPurchase.setAdapter(simpleAdapter);
        listPurchase.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String harga = ((TextView)view.findViewById(R.id.text_title)).getText().toString();
                Toast.makeText(getActivity(), harga, Toast.LENGTH_SHORT).show();

                startActivity(new Intent(getActivity(), StatusActivity.class));

            }
        });
    }

}
